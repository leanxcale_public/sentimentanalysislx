from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import sessionmaker
import pandas as pd
import re
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from wordcloud import WordCloud
import numpy as np
import matplotlib.pyplot as plt
from textblob import TextBlob




def lx_to_df(user, database, table):
    engine = create_engine('leanxcale://' + user + '@10.64.21.1:1529/' + database)
    Session = sessionmaker(bind=engine)
    session = Session()
    meta = MetaData()
    meta.bind = engine
    tweetsTable = Table(table, meta, autoload='True')
    query_all = session.query(tweetsTable)
    df = pd.read_sql(query_all.statement, query_all.session.bind)
    print('df loaded from LX DB')
    engine.dispose()
    return df


def lx_to_df_2(user, database, SQL):
    engine = create_engine('leanxcale://' + user + '@10.64.21.1:1529/' + database)
    df = pd.read_sql_query(SQL, engine)
    engine.dispose()
    return df


def clean_tweets(df):
    stopwords_en = stopwords.words('english')
    # ps = PorterStemmer()
    wordnet_lemmatizer = WordNetLemmatizer()
    df["clean_tweets"] = None
    df['len'] = None
    print('cleaning tweets')
    for i in range(0, len(df['TEXT'])):
        exclusion_list = ['[^a-zA-Z]', 'rt', 'http', 'co', 'RT']
        exclusions = '|'.join(exclusion_list)
        text = re.sub(exclusions, ' ', df['TEXT'][i])
        text = text.lower()
        words = text.split()
        words = [wordnet_lemmatizer.lemmatize(word) for word in words if not word in stopwords_en]
        # words = [ps.stem(word) for word in words]
        #df.loc('clean_tweets')[i] = ' '.join(words)
        df['clean_tweets'][i] = ' '.join(words)
    df['len'] = np.array([len(tweet) for tweet in df["clean_tweets"]])
    return df


def sentiment(tweet):
    analysis = TextBlob(tweet)
    if analysis.sentiment.polarity > 0:
        return 1
    elif analysis.sentiment.polarity == 0:
        return 0
    else:
        return -1


def word_cloud(df):
    plt.subplots(figsize=(12, 10))
    wordcloud = WordCloud(
        background_color='white',
        width=1000,
        height=800).generate(" ".join(df['clean_tweets']))
    plt.imshow(wordcloud)
    plt.axis('off')


if __name__ == "__main__":
    df = lx_to_df('APP', 'twitterdb', 'TWEETS')
    # SQL = 'select * from TWEETS'
    # df = lx_to_df_2('APP', 'twitterdb', SQL)
    clean_tweets(df)
    df['Sentiment'] = np.array([sentiment(x) for x in df['clean_tweets']])
    pos_tweets = [tweet for index, tweet in enumerate(df["clean_tweets"]) if df["Sentiment"][index] > 0]
    neg_tweets = [tweet for index, tweet in enumerate(df["clean_tweets"]) if df["Sentiment"][index] < 0]
    neu_tweets = [tweet for index, tweet in enumerate(df["clean_tweets"]) if df["Sentiment"][index] == 0]

    print("percentage of positive tweets: {}%".format(100 * (len(pos_tweets) / float(len(df['clean_tweets'])))))
    print("percentage of negative tweets: {}%".format(100 * (len(neg_tweets) / float(len(df['clean_tweets'])))))
    print("percentage of neutral tweets: {}%".format(100 * (len(neu_tweets) / float(len(df['clean_tweets'])))))

    word_cloud(df[df.Sentiment == 1])
    word_cloud(df[df.Sentiment == -1])
    word_cloud(df[df.Sentiment == 0])
    plt.show()
